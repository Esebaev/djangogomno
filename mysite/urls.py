from django.conf.urls import url
from django.contrib import admin

from mysite.views import BaseView

urlpatterns = {
    url(r'^base/', BaseView.as_view()),
    url(r'^admin/', admin.site.urls),
}
